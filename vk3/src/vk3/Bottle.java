package vk3;

public class Bottle {
	private String name= "Pepsi Max";
	private String manufacturer= "Pepsi";
	private int total_energy=0;
	private double size=0.5;
	private double price=1.8;
	
	public Bottle(String n, String manuf, int totE, double s, double p) {
		name=n;
		manufacturer=manuf;
		total_energy=totE;
		size=s;
		price=p;
	}
public String getName() {
	return name;
}
public String getManufacture() {
	return manufacturer;
}
public int getEnergy() {
	return total_energy; 
}
public double getSize() {
	return size;
}
public double getPrice() {
	return price;
}
}
