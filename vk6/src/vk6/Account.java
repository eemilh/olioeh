package vk6;


abstract class Account {
	protected String tilinumero;
	protected double saldo=0.0;
	protected double luottoraja=0;
	
	Account(String n, double rahamaara, double luottoraja) {
		tilinumero=n;
		saldo=rahamaara;
		luottoraja=luottoraja;
		
	}
	public void talleta(double rahaa) {
		saldo=saldo+rahaa;
	}
	
	public void nosta(double rahaa) {
		
	}

}
class K�ytt�tili extends Account {
	K�ytt�tili(String n, double rahamaara, double luottoraja) {
		super(n, rahamaara, luottoraja);
	}
	public void nosta(double rahaa) {
		if (saldo>=rahaa) {
			saldo=saldo-rahaa;
		}
		else {
			System.out.println("Saldo ei riit�");
		}
	}
}
class Luottotili extends Account {
	Luottotili(String n, double rahamaara, double luotto) {
		super(n, rahamaara, luotto);
		luottoraja=luotto;
	}
	public void nosta(double rahaa) {
		if (saldo+luottoraja>=rahaa) {
			saldo=saldo-rahaa;
		}
		else {
			System.out.println("Luotto ei riit�");
		}
}
}