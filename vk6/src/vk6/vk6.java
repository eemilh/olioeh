package vk6;

import java.util.Locale;
import java.util.Scanner;
public class vk6 {


	public static void main(String[] args) {
		double rahaa=0.0;
		Account temp;
		Bank bank=new Bank();
		
		Scanner input=new Scanner(System.in);
		input.useLocale(Locale.UK);
		
		int selection;
		do
		{
			System.out.println("");
			System.out.println("*** PANKKIJ�RJESTELM� ***");
			System.out.println("1) Lis�� tavallinen tili");
			System.out.println("2) Lis�� luotollinen tili");
			System.out.println("3) Tallenna tilille rahaa");
			System.out.println("4) Nosta tililt�");
			System.out.println("5) Poista tili");
			System.out.println("6) Tulosta tili");
			System.out.println("7) Tulosta kaikki tilit");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			selection = input.nextInt();
			switch(selection) {
			case 1:
				bank.addlisaatili();
				
				break;
			case 2:
				bank.addluottotili();
				break;
			case 3:
				System.out.print("Sy�t� tilinumero: ");
				
				if ((temp=bank.etsiTili(input.next()))!=null) {
					System.out.print("Sy�t� raham��r�: ");
					rahaa = Double.parseDouble(input.next());
					bank.talleta(temp, rahaa);
				}
				else {System.out.println("Tili� ei l�ydy");}
				break;
			case 4:
				System.out.print("Sy�t� tilinumero: ");
				if ((temp=bank.etsiTili(input.next()))!=null) {
					System.out.print("Sy�t� raham��r�: ");
					rahaa = input.nextDouble();
					bank.nosta(temp, rahaa);
				}
				else {System.out.println("Tili� ei l�ydy");}
				break;
			case 5:
				System.out.print("Sy�t� poistettava tilinumero: ");
				bank.poista(input.next());
				System.out.println("Tili poistettu.");
				break;
			case 6:
				System.out.print("Sy�t� tulostettava tilinumero: ");
				temp=bank.etsiTili(input.next());
				if (temp!=null) {
					bank.tulostaTili(temp);
				}
				else {System.out.println("Tili� ei l�ydy");}
				break;
			case 7:
				System.out.println("Kaikki tilit:");
				bank.tulostaTilit();
				break;
			case 0:
				break;
			default:
				System.out.println("Valinta ei kelpaa.");
				break;
			}
			
		}
		while(selection != 0);
		input.close();
	}

}

