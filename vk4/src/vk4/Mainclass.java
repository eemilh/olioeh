package vk4;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.io.IOException;

public class Mainclass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//ReadAndWriteIO io= new ReadAndWriteIO();
		//io.readAndWrite("testi.txt", "output.txt");
		try {
			ZipFile zipFile = new ZipFile("zipinput.zip");
			ZipEntry zipEntry = zipFile.entries().nextElement();
			InputStream inputStream = zipFile.getInputStream(zipEntry);
			String result = new BufferedReader(new InputStreamReader(inputStream)).lines().collect(Collectors.joining("\n"));
			System.out.println(result);
			zipFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
