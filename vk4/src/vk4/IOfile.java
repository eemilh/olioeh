package vk4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class IOfile {


		public void readFile(String s) {
			try {
				BufferedReader br = new BufferedReader(new FileReader(s));
				String out="";
				while ((out=br.readLine())!=null) {
				System.out.println(br.readLine());
				}
				br.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
		
 public void writeFile(String s, String text) {
	 BufferedWriter bw;
	try {
		bw = new BufferedWriter(new FileWriter (s));
		bw.write(text);
		 bw.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
	 
 }

 public void readAndWrite(String rf, String wf) {
	boolean vletter=false; 
	try {
		BufferedReader br = new BufferedReader(new FileReader(rf));
		BufferedWriter bw = new BufferedWriter(new FileWriter (wf));
		String out="";
		while ((out=br.readLine())!=null) {
			for (int i = 0; i < out.length(); i++) {
				if ((out.charAt(i)=='v') || (out.charAt(i)=='V')) {
					vletter=true;
					break;
				}
				
			}
			if ((out.trim().isEmpty()==false) && (out.length()<30) && vletter) {
			bw.write(out);
			bw.newLine();
			}
			vletter=false;
		}

		 bw.close();
		 br.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
 }
	}
  
