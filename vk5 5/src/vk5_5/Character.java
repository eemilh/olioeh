package vk5_5;

abstract class Character {
	public WeaponBehavior weapon;
	//protected String character;
	protected void fight() {
		System.out.println(this.getClass().getSimpleName( )+" tappelee aseella " + this.weapon.weapon_name);
	}
}

class King extends Character {
	public King(int ase) {
	//character = "King";
		if (ase == 1){
			weapon=new KnifeBehavior();		
		}
		if (ase == 2) {
			weapon=new AxeBehavior();
		}
		if (ase == 3) {
			weapon=new SwordBehavior();
		}
		if (ase == 4) {
			weapon=new ClubBehavior();
		}
	}
}
class Knight extends Character {
	public Knight(int ase) {
	//character = "King";
		if (ase == 1){
			weapon=new KnifeBehavior();
		}
		if (ase == 2) {
			weapon=new AxeBehavior();
		}
		if (ase == 3) {
			weapon=new SwordBehavior();
		}
		if (ase == 4) {
			weapon=new ClubBehavior();
		}
	}
}
class Queen extends Character {
	public Queen(int ase) {
	//character = "King";
		if (ase == 1){
			weapon=new KnifeBehavior();
		}
		if (ase == 2) {
			weapon=new AxeBehavior();
		}
		if (ase == 3) {
			weapon=new SwordBehavior();
		}
		if (ase == 4) {
			weapon=new ClubBehavior();
		}
	}
}
class Troll extends Character {
	public Troll(int ase) {
	//character = "King";
		if (ase == 1){
			weapon=new KnifeBehavior();	
		}
		if (ase == 2) {
			weapon=new AxeBehavior();
		}
		if (ase == 3) {
			weapon=new SwordBehavior();
		}
		if (ase == 4) {
			weapon=new ClubBehavior();
		}
	}
}

abstract class WeaponBehavior {
	public String weapon_name;
	protected void useWeapon() {
		//System.out.println(this.getClass().getSimpleName( )+"taistelee aseella" + this.weapon.getClass().getSimpleName());
	}
}

class KnifeBehavior extends WeaponBehavior {
	public KnifeBehavior() {
		weapon_name="Knife";
	}
}
class AxeBehavior extends WeaponBehavior {
	public AxeBehavior() {
			weapon_name="Axe";
	}
}
class SwordBehavior extends WeaponBehavior {
	public SwordBehavior() {
				weapon_name="Sword";
	}
}
class ClubBehavior extends WeaponBehavior {
	public ClubBehavior() {
					weapon_name="Club";
	}
}
