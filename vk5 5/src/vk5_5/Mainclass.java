package vk5_5;

import java.util.Scanner;

public class Mainclass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Character hahmo = null;
		
		Scanner input=new Scanner(System.in);
		int selection;
		do {
			System.out.println("*** TAISTELUSIMULAATTORI ***");
			System.out.println("1) Luo hahmo");
			System.out.println("2) Taistele hahmolla");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			selection = input.nextInt();
			switch(selection) {
				case 1:
					System.out.println("Valitse hahmosi: ");
					System.out.println("1) Kuningas");
					System.out.println("2) Ritari");
					System.out.println("3) Kuningatar");
					System.out.println("4) Peikko");
					System.out.print("Valintasi: ");
					selection = input.nextInt();
					switch(selection) {
						case 1:
							System.out.println("Valitse aseesi: ");
							System.out.println("1) Veitsi");
							System.out.println("2) Kirves");
							System.out.println("3) Miekka");
							System.out.println("4) Nuija");
							System.out.print("Valintasi: ");
							selection = input.nextInt();
							King king = new King(selection);
							hahmo=king;
							
							break;
						case 2:
							System.out.println("Valitse aseesi: ");
							System.out.println("1) Veitsi");
							System.out.println("2) Kirves");
							System.out.println("3) Miekka");
							System.out.println("4) Nuija");
							System.out.print("Valintasi: ");
							selection = input.nextInt();
							Knight knight = new Knight(selection);
							hahmo=knight;
				
							break;
						case 3:
							System.out.println("Valitse aseesi: ");
							System.out.println("1) Veitsi");
							System.out.println("2) Kirves");
							System.out.println("3) Miekka");
							System.out.println("4) Nuija");
							System.out.print("Valintasi: ");
							selection = input.nextInt();
							Queen queen = new Queen(selection);
							hahmo=queen;
							break;
						case 4:
							System.out.println("Valitse aseesi: ");
							System.out.println("1) Veitsi");
							System.out.println("2) Kirves");
							System.out.println("3) Miekka");
							System.out.println("4) Nuija");
							System.out.print("Valintasi: ");
							selection = input.nextInt();
							Troll troll = new Troll(selection);
							hahmo=troll;
							break;
					}
					
					break;
				case 2:
					hahmo.fight();
		
		
					break;
			
				case 0:
					
					break;
			}
		}
		while(selection != 0);	
		input.close();
	}

}
